/*
Türeyen sınıf yani alt sınıflar ana(üst) sınıfın tüm özelliklerini ve
metotlarını aynı işlevi gösterecek şekilde kullanabilme ve
kendine ait yeni özellikler barındırabilmelidir.
 */

/*
Bakıldığında kare ve dikdörtgenin alan hesabı aynıdır. Ancak
uzunluklar değiştirilmek istendiğinde kare için uzunluk veya genişlik değerini
ayrı ayrı vermek bir anlam ifade etmeyecektir.
Dolayısıyla ortak bir interface oluşturulup kare ve dikdörtgenin geometri olduğunu
belirtiyoruz. Ardından sınıfa özel setterları ait oldukları sınıfta yazıyoruz
 */

interface Shape {
    area(): number;
}

export class Square implements  Shape {
    size: number

    constructor(size: number) {
        this.size = size;
    }

    area(): number {
        return this.size * this.size;
    }

    setSize(size:number):void{
        this.size = size
    }
}

export class Rectangle implements Shape {
    width: number;
    height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }

    area(): number {
        return this.width * this.height;
    }

    setWidth(width:number):void{
        this.width = width
    }
    setHeight(height:number):void{
        this.height = height
    }
}