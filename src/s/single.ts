/*
Her sınıfın sadece tek bir sorumluluğu olmalıdır.
Birbirinden bağımsız olayları ayırıp, anlaşılabilirliği arttırmalıyız.
 */


/*
Userin adını emailini almak ile email göndermek ayrı işlerdir.
Dolayısyla User classının içinde tanımlamayıp yeni bir sınıf oluşturduk
 */

/*
DEPENDENCY INVERSION PRINCIPLE
Sınıflar arası bağımlılıklar olabildiğince az olmalıdır
özellikle üst seviye sınıflar alt seviye sınıflara bağımlı olmamalıdır.

 */
class EmailService {
    sendEmail():void {
        console.log('Email sent..');
    }
}

export class User {
    name: string;
    email: string;

    constructor(name: string, email: string ) {
        this.name = name;
        this.email = email;
    }

    // Email gönderme işi service çağırıyoruz.
    // Dependecy inversiona örnek
    emailService: EmailService = new EmailService()

    getUserName():string {
        return this.name;
    }

    getUserEmail():string{
        return this.email;
    }

    sendEmail():void {
        this.emailService.sendEmail();
    }
}

