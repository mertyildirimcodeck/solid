/*
Sorumlulukların hepsini tek bir arayüze toplamak yerine
daha özelleştirilmiş birden fazla arayüz
oluşturmayı tercih etmemizi söyleyen prensiptir.

 */

//Eğer burada uçmak veya havlamak gibi özel bir tanımlama yapsaydık
//Animal sınıfından türeyen Bird sınıfı çalışmayacaktı.
class Animal {
    name: string
    kind: string

    constructor(name: string, kind: string) {
        this.name = name
        this.kind = kind
    }
}

export class Bird extends  Animal {
    fly() {
        console.log('I am flying')
    }
}

export class Dog extends Animal {
    bark() {
        console.log('I am barking')
    }
}

