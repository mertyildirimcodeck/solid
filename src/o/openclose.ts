/*
Bir sınıf ya da fonksiyon var olan özellikleri korumalı
yani davranışını değiştirmiyor olmalı ve
yeni özellikler kazanabilmelidir.
 */


//Burada geometrilerden alanı istediğimizi belirttik. Ancak nasıl yapacaklarına karışmadık.
//Daire veya Dikdörtgen için Shape'i implemente ederek kendi içlerinde alan hesaplamaları yaptık
//Böylelikle ileride ne tarz bir geometri eklenirse eklensin source kodumuzu değiştirmeden
//yeni geometriler ekleyebileceğiz.

interface Shape{
    getArea():number
}

export class Rectangle implements Shape{
    x: number;
    y: number;

    constructor(x: number, y: number){
        this.x = x
        this.y = y;
    }

    getArea(): number {
        return this.x* this.y;
    }
}


export class Circle implements Shape{
    radius: number;

    constructor(radius: number){
        this.radius = radius;
    }

    getArea(): number {
        return Math.PI * this.radius * this.radius;
    }
}