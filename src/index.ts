import {Dog, Bird} from "./i/interfacesegregation";
import {Circle, Rectangle} from "./o/openclose";
import {User} from "./s/single";
import {Square, Rectangle as R} from "./l/liskov";



//--------- Single Responsibility Principle --------
const usr1: User = new User("John", "Doe")
usr1.sendEmail()
//--------- Single Responsibility Principle -------- #


//--------- Interface Segregation Principle ----------
const dog: Dog = new Dog("Rex", "Golden")
const bird: Bird = new Bird("Asd", "Muhabbet")

dog.bark()
bird.fly()
//--------- Interface Segregation Principle ---------- #


//--------- Open Close Principle ----------
const rect: Rectangle = new Rectangle(4,5)
console.log(rect.getArea())

const circ: Circle = new Circle(4)
console.log(circ.getArea())
//--------- Open Close Principle ---------- #


//--------- Liskov Substitution Principle ----------
const squ: Square = new Square(12)
squ.setSize(4)
console.log(squ.area())

const rectL: R = new R(4,5)
rectL.setHeight(3)
rectL.setWidth(4)
console.log(rectL.area())


//--------- Liskov Substitution Principle ---------- #
